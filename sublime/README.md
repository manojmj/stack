## Usage

## Restoring

1. Install [Sublime Text 3](https://www.sublimetext.com/3)
2. Install [Sublime Package Manager](https://packagecontrol.io/installation)
3. Install the [PackageSync](https://packagecontrol.io/packages/PackageSync) package.
4. Copy the backed up `SublimePackagesBackup` file to a folder on your machine.
4. Run "Package Sync - restore from zip file"

## Backing up

1. Run "Package Sync - backup to a zip file"
1. Add the generated zip file to this folder.
1. `git push`
1. Restore when necessary

## Theme

https://github.com/equinusocio/material-theme

## Settings

```json
{
"always_show_minimap_viewport": true,
"bold_folder_labels": true,
"color_scheme": "Packages/Material Theme/schemes/Material-Theme-Darker.tmTheme",
"ensure_newline_at_eof_on_save": true,
"font_size": 18,
"ignored_packages":
[
"Vintage"
],
"rulers":
[
80
],
"tab_size": 2,
"theme": "Material-Theme-Darker.sublime-theme",
"translate_tabs_to_spaces": true,
"trim_trailing_white_space_on_save": true
}
```

## Linter

Install:
- https://github.com/SublimeLinter/SublimeLinter (via package manager)
- https://github.com/SublimeLinter/SublimeLinter-rubocop (via package manager)

- config:

```json
// SublimeLinter Settings - User
// SublimeLinter Settings - User
{
  "linters": {
      "rubocop": {
         "use_bundle_exec": true,
         "env": {
            "PATH": "~/.asdf/shims:$PATH"
         },
         "args": ["--config", ".rubocop.yml"]
      }
  },
  "debug": true
}
```

## Others

- https://packagecontrol.io/packages/SideBarEnhancements
- https://github.com/randy3k/Terminus
