# Stack

Some dotfiles and other extras that I use as my daily driver.


## Dev Environment

- [Sublime Text 3](sublime/README.md)
- [iTerm 2](https://www.iterm2.com/downloads.html), with Rails & git plugin.
- [ohmyzsh](https://github.com/ohmyzsh/ohmyzsh#via-curl)
- [Tower 2](https://www.git-tower.com/download-TO2M) (License in Email)

## Browser

- [Brave Browser](https://brave.com/)

## Some others

- [Franz](https://meetfranz.com/)
- [Dashlane Password Manager](https://www.dashlane.com/)
- [VLC Media Player](https://www.videolan.org/index.html)


