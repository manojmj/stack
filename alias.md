alias rbm="git pull origin master --rebase"

alias db="gdk start postgresql"

alias r="gdk start"

alias s="gdk stop"

alias setup="bundle && rdm && rdmt"

alias restart="s && r"

alias rdmt="rdm RAILS_ENV=test"

alias spec="bin/rspec"
